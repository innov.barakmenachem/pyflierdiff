
# Image similariry
*** all credits to: https://gist.github.com/duhaime/211365edaddf7ff89c0a36d9f3f7956c ***

This is an image similarity algorithem they givs as a result   4 parameters:
`structural_sim, pixel_sim, sift_sim, earth_movers_distance`

if 2 images are exactlly the same, the its values shoiuld be:
`1, 0, 1, 0`


for example, if we run the following commands:


## Test 1:
`$ python3 measure_img_similarity.py` - 1,1 exactly the same picture:
1.png 1.png
1.0 0.0 1.0 0.0

`$ python3 measure_img_similarity.py` - 5 is same picture as 1 but we rotate it and change some colors:
1.png 5.png
0.004920330605915709 0.2527802822636623 0.9040590405904059 0.0009156614542007446

`$ python3 measure_img_similarity.py` - 2 is same picture as 1 but we just rotate it:
1.png 2.png
0.005644945160313451 0.25276736839144837 0.9562289562289562 0.0003010407090187073

`$ python3 measure_img_similarity.py` - 4 is totally dirrerent picture then 1:
1.png 4.png
0.0038320879295043737 0.4204807356292126 0.3482142857142857 0.004864640533924103


`$ python3 measure_img_similarity.py` - 1_1 is same picture as 1 but we add some text on it:
1.png 1_1.png
0.8925006559609372 0.06384566063974417 0.9978448275862069 0.0003266260027885437


## Test 1 - pics in directory "pics1" (first line is most similar to last line that its totally differet image):

		        structural_sim        pixel_sim            sift_sim            earth_movers_distance
1.png <> 1.png		1.0                   0.0                  1.0                 0.0
1.png <> 1_1.png	0.8925006559609372    0.06384566063974417  0.9978448275862069  0.0003266260027885437
1.png <> 2.png		0.005644945160313451  0.25276736839144837  0.9562289562289562  0.0003010407090187073
1.png <> 5.png		0.004920330605915709  0.2527802822636623   0.9040590405904059  0.0009156614542007446
1.png <> 4.png		0.0038320879295043737 0.4204807356292126   0.3482142857142857  0.004864640533924103




## Test 2 -  pics in directory "pics2"(first line is most similar to last two lines that its totally differet images)
		        structural_sim        pixel_sim            sift_sim            earth_movers_distance
1.png <> 1.png		1.0                   0.0                  1.0                 0.0
1.png <> 1_1.png	0.8865919890027354    0.06007509044572419  0.9897172236503856  0.0005387365818023682
1.png <> 2.png		0.4660208281457919    0.11004210827397365  0.9490740740740741  0.0018357709050178528
1.png <> 3.jpg		-0.002517832206236234 0.22231864181219363  0.6167664670658682  0.000780247151851654
1.png <> 4.png		0.00253241739068506   0.22149807051116344  0.6234567901234568  0.0007603764533996582




## Results:

we can infer that, to compare between two ads that differnet a littel 
in colors or some text on it, the parameters values should be:

1. structural_sim - not good indicator.
2. pixel_sim  - not good indicator.
3. sift_sim > 0.8
4. earth_movers_distance - not good indicator.

if all fo this 4 equlitys sutisfied its means that the images are similar.

